import './assets/main.css'

import { createApp } from 'vue'
import { createPinia } from 'pinia'
import axiosApiClient from '@/plugins/axios-client'

import App from './App.vue'
import router from './router'
import httpClientInjectionKey from '@/core/http/http-client.symbol'

const app = createApp(App)

app.config.errorHandler = (err) => {
  console.warn('Error:', err)
}

app.provide(httpClientInjectionKey, axiosApiClient)
app.use(createPinia())
app.use(router)
app.mount('#app')
