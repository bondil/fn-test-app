import axiosInstance from '@/plugins/axios'
import { AxiosHttpClient } from '@/core/http/axios-http-client'

const axiosApiClient = new AxiosHttpClient(axiosInstance)
export default axiosApiClient
