import axios from 'axios'
import type { AxiosInstance, AxiosRequestConfig } from 'axios'

const axiosConfig: AxiosRequestConfig = {
  responseType: 'json',
  headers: { 'content-type': 'application/json' }
}

const axiosInstance: AxiosInstance = axios.create(axiosConfig)
export default axiosInstance
