import { computed, type ComputedRef } from 'vue'

type PaginationOptions = {
  currentPage: number
  pageRange: number
  pagesCount: number
  marginPages: number
}

type PaginationItem = {
  index: number
  content?: number
  selected?: boolean
  disabled?: boolean
  breakView?: boolean
}

const generatePagination = (options: PaginationOptions): PaginationItem[] => {
  const { currentPage, pageRange, pagesCount, marginPages } = options
  const items: PaginationItem[] = []

  const addItem = (index: number) => {
    items[index] = {
      index,
      content: index + 1,
      selected: index === currentPage - 1
    }
  }

  const setBreakView = (index: number) => {
    items[index] = { index, disabled: true, breakView: true }
  }

  if (pagesCount <= pageRange) {
    for (let i = 0; i < pagesCount; i += 1) {
      addItem(i)
    }
  } else {
    const halfPageRange = Math.floor(pageRange / 2)

    for (let i = 0; i < marginPages; i += 1) {
      addItem(i)
    }

    let selectedRangeLow = 0

    if (currentPage - halfPageRange > 0) {
      selectedRangeLow = currentPage - 1 - halfPageRange
    }

    let selectedRangeHigh = selectedRangeLow + pageRange - 1

    if (selectedRangeHigh >= pagesCount) {
      selectedRangeHigh = pagesCount - 1
      selectedRangeLow = selectedRangeHigh - pageRange + 1
    }

    for (
      let i = selectedRangeLow;
      i <= selectedRangeHigh && i <= pagesCount - 1;
      i += 1
    ) {
      addItem(i)
    }

    if (selectedRangeLow > marginPages) {
      setBreakView(selectedRangeLow - 1)
    }

    if (selectedRangeHigh + 1 < pagesCount - marginPages) {
      setBreakView(selectedRangeHigh + 1)
    }

    for (let i = pagesCount - 1; i >= pagesCount - marginPages; i -= 1) {
      addItem(i)
    }
  }

  return items.filter(Boolean)
}

const usePagination = (options: ComputedRef<PaginationOptions>) => {
  const pages = computed(() => generatePagination(options.value))

  return { pages }
}

export default usePagination
