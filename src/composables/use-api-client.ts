import { inject } from 'vue'
import httpClientInjectionKey from '@/core/http/http-client.symbol'
import type HttpClient from '@/core/http/http-client'

export default () => {
  const apiClient = inject(httpClientInjectionKey) as HttpClient

  return {
    apiClient
  }
}
