import HttpClient, { type ApiResponse } from '@/core/http/http-client'
import type { NpmPackage } from '@/modules/packages/types/packages.types'

type JsDeliverApiService = {
  queryPackageInfo(name: string): Promise<ApiResponse<NpmPackage>>
  queryPackageBadge(name: string): Promise<ApiResponse<string>>
}

const JSDELIVER_API_BASE_URL = 'https://data.jsdelivr.com/v1/'

const jsDeliverApiService = (apiClient: HttpClient): JsDeliverApiService => {
  const queryPackageInfo = async (packageName: string) => {
    return await apiClient.get<NpmPackage>(`packages/npm/${packageName}`, {
      baseURL: JSDELIVER_API_BASE_URL
    })
  }

  const queryPackageBadge = async (packageName: string) => {
    return await apiClient.get<string>(
      `stats/packages/npm/${packageName}/badge`,
      {
        baseURL: JSDELIVER_API_BASE_URL
      }
    )
  }

  return {
    queryPackageInfo,
    queryPackageBadge
  }
}

export default jsDeliverApiService
