import type {
  NpmPackagesSearchResult,
  NpmPackageMetadata
} from '@/modules/packages/types/packages.types'
import HttpClient, { type ApiResponse } from '@/core/http/http-client'

type NpmApiService = {
  queryPackagesByName(
    text: string,
    page: number,
    limit: number
  ): Promise<ApiResponse<NpmPackagesSearchResult>>
  queryPackageMetadata(name: string): Promise<ApiResponse<NpmPackageMetadata>>
}

const NPM_API_BASE_URL = 'https://registry.npmjs.org/'

const npmApiService = (apiClient: HttpClient): NpmApiService => {
  const queryPackagesByName = async (text: string, page = 1, limit = 10) => {
    return await apiClient.get<NpmPackagesSearchResult>(`-/v1/search`, {
      baseURL: NPM_API_BASE_URL,
      params: {
        text,
        from: page,
        size: limit
      }
    })
  }

  const queryPackageMetadata = async (packageName: string) => {
    return await apiClient.get<NpmPackageMetadata>(packageName, {
      baseURL: NPM_API_BASE_URL
    })
  }

  return {
    queryPackagesByName,
    queryPackageMetadata
  }
}

export default npmApiService
