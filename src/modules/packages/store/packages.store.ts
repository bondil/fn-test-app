import { computed, ref, shallowRef } from 'vue'
import { defineStore } from 'pinia'
import useApiClient from '@/composables/use-api-client'
import npmApiService from '@/modules/packages/services/npm-api.service'
import { PackageBrief } from '@/modules/packages/package.model'

export const usePackagesStore = defineStore('packages', () => {
  const { apiClient } = useApiClient()
  const { queryPackagesByName } = npmApiService(apiClient)

  const packages = shallowRef<PackageBrief[]>()
  const packagesCount = ref(0)
  const currentPage = ref(0)
  const perPage = ref(10)
  const isLoading = ref(false)
  const pagesCount = computed(() =>
    Math.floor(packagesCount.value / perPage.value)
  )

  const queryPackages = async (text: string, page: number = 1) => {
    isLoading.value = true

    try {
      const response = await queryPackagesByName(
        text,
        Number(page),
        perPage.value
      )
      if (!response.success) throw response.error

      packages.value = response.result.objects.map(
        (p) => new PackageBrief(p.package)
      )
      packagesCount.value = response.result.total
      currentPage.value = page
    } catch (e: any) {
      console.warn('error', e)
    } finally {
      isLoading.value = false
    }
  }

  return {
    packages,
    packagesCount,
    currentPage,
    perPage,
    pagesCount,
    isLoading,
    queryPackages
  }
})
