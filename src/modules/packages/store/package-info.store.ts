import { ref } from 'vue'
import { defineStore } from 'pinia'
import useApiClient from '@/composables/use-api-client'
import jsDeliverApiService from '@/modules/packages/services/jsdeliver-api.service'
import npmApiService from '@/modules/packages/services/npm-api.service'
import type { ApiResponse } from '@/core/http/http-client'
import type {
  NpmPackage,
  NpmPackageMetadata
} from '@/modules/packages/types/packages.types'
import { PackageFull } from '@/modules/packages/package.model'

const unwrapResponseResult = <T>(
  r: PromiseSettledResult<ApiResponse>
): T | undefined => {
  if (r.status === 'fulfilled' && r.value.success) {
    return r.value.result
  }
}

export const usePackageInfoStore = defineStore('package-info', () => {
  const { apiClient } = useApiClient()
  const { queryPackageInfo, queryPackageBadge } = jsDeliverApiService(apiClient)
  const { queryPackageMetadata } = npmApiService(apiClient)
  const packageInfo = ref<PackageFull>()
  const isLoading = ref(false)

  const getPackageInfo = async (name: string) => {
    isLoading.value = true

    try {
      const responses = await Promise.allSettled([
        queryPackageInfo(name),
        queryPackageMetadata(name),
        queryPackageBadge(name)
      ])
      const [packageResp, metadataResp, badgeResp] = responses

      const packageData = unwrapResponseResult<NpmPackage>(packageResp)
      const packageMeta = unwrapResponseResult<NpmPackageMetadata>(metadataResp)
      const packageBadge = unwrapResponseResult<string>(badgeResp)

      packageInfo.value = new PackageFull(
        packageData,
        packageMeta,
        packageBadge
      )
    } catch (e: any) {
      console.warn('error', e)
    } finally {
      isLoading.value = false
    }
  }

  return {
    packageInfo,
    isLoading,
    getPackageInfo
  }
})
