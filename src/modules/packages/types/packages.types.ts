type MaintainerInfo = {
  email: string
  username: string
  url: string
}

type Maintainer = MaintainerInfo & {
  name: string
}

type PackageLinks = Partial<{
  bugs: string
  homepage: string
  npm: string
  repository: string
}>

type NpmPackageMetadata = {
  author: { name: string }
  bugs: { url: string }
  description: string
  'dist-tags': {
    latest: string
  }
  homepage: string
  keywords: string[]
  license: string
  maintainers: MaintainerInfo[]
  name: string
  repository: {
    type: string
    url: string
  }
  time: {
    created: string
    modified: string
    [key: string]: string
  }
  versions: {
    [version: string]: NpmPackageMetadata
  }
}

type NpmPackage = {
  name: string
  author: Maintainer
  date: string
  description: string
  keywords: string[]
  links: PackageLinks
  maintainers: MaintainerInfo[]
  publisher: MaintainerInfo
  scope: string
  version: string
}

type NpmApiPackage = {
  [key: string]: any
  package: NpmPackage
}

type NpmPackagesSearchResult = {
  objects: NpmApiPackage[]
  time: string
  total: number
}

type PackageBadge = string

export {
  type PackageLinks,
  type MaintainerInfo,
  type Maintainer,
  type NpmPackage,
  type NpmPackageMetadata,
  type NpmPackagesSearchResult,
  type PackageBadge
}
