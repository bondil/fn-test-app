import type {
  MaintainerInfo,
  NpmPackage,
  NpmPackageMetadata,
  PackageLinks
} from '@/modules/packages/types/packages.types'

class PackageFull {
  name: string
  author: string
  date: string
  description: string
  keywords: string[]
  links: PackageLinks | undefined
  maintainers: MaintainerInfo[]
  publisher: MaintainerInfo | undefined
  scope: string
  version: string
  homepage: string
  license: string
  repository: NpmPackageMetadata['repository'] | undefined
  time: NpmPackageMetadata['time'] | undefined
  badge?: string | undefined

  constructor(info?: NpmPackage, meta?: NpmPackageMetadata, badge?: string) {
    this.name = info?.name ?? meta?.author?.name ?? ''
    this.author = info?.author?.name ?? meta?.author?.name ?? ''
    this.date = this.getLocaleDate(info?.date) ?? ''
    this.description = info?.description ?? meta?.description ?? ''
    this.keywords = info?.keywords ?? meta?.keywords ?? []
    this.links = info?.links
    this.maintainers = info?.maintainers ?? meta?.maintainers ?? []
    this.publisher = info?.publisher
    this.scope = info?.scope ?? ''
    this.version = info?.version ?? ''
    this.homepage = meta?.homepage ?? ''
    this.license = meta?.license ?? ''
    this.repository = meta?.repository
    this.time = meta?.time
    this.badge = badge
  }

  private getLocaleDate(date: string | undefined) {
    if (date) return new Date(date).toLocaleString('en')
  }
}

class PackageBrief {
  name: {
    text: string
    url: string | undefined
  }
  author: {
    text: string
    url: string | undefined
  }
  description: string
  version: string

  constructor(dto: NpmPackage) {
    this.name = {
      text: dto.name,
      url: dto?.links?.homepage ?? dto?.links?.repository
    }
    this.author = {
      text: dto.author?.name || dto.author?.username || dto.author?.email,
      url: dto.author?.url
    }
    this.description = dto.description ?? '-'
    this.version = dto.version
  }
}

export { PackageBrief, PackageFull }
