type ApiResponse<T = any> = { success: true; result: T } | { success: false; error: Error }

abstract class HttpClient {
  abstract get<T = any>(...args: any[]): Promise<ApiResponse<T>>
}

export default HttpClient
export type { ApiResponse }
