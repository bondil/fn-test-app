import HttpClient, { type ApiResponse } from '@/core/http/http-client'
import type { AxiosError, AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios'

class AxiosHttpClient implements HttpClient {
  constructor(private http: AxiosInstance) {}

  async get<T>(endpoint: string, options?: AxiosRequestConfig) {
    return this.handleResponse<T>(this.http.get<T>(endpoint, options))
  }

  protected async handleResponse<T>(response: Promise<AxiosResponse<T>>): Promise<ApiResponse<T>> {
    const result = await response.catch((error: Error | AxiosError) => error)

    if (result instanceof Error) {
      const error = {
        name: result.name,
        message: result.message,
        code: (result as AxiosError)?.code
      }

      return { success: false, error }
    }

    return {
      success: true,
      result: (result as AxiosResponse).data
    }
  }
}

export { AxiosHttpClient }
