import type { InjectionKey } from 'vue'
import type HttpClient from '@/core/http/http-client'

const httpClientInjectionKey = Symbol() as InjectionKey<HttpClient>
export default httpClientInjectionKey
