export default (cb: (...args: any[]) => unknown, timeout = 200) => {
  let timeoutId: number

  const clearTimeout = () => {
    if (timeoutId) window.clearTimeout(timeoutId)
  }

  return (...args: any[]) => {
    clearTimeout()

    timeoutId = setTimeout(async () => {
      await cb(...args)
      clearTimeout()
    }, timeout)
  }
}
